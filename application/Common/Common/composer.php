<?php

/**
 * 初始化功能扩展模块
 * Created by PhpStorm.
 * User: Mo
 * Date: 15-10-15
 * Time: 上午1:12
 */

// 加载 composer 支持
require SITE_PATH.'vendor/autoload.php';

// 执行初始化
(new App\System)->init();


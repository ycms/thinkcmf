<?php namespace App;

use Def;
use Think\Hook;

/**
 * 系统功能扩展
 * User: Mo
 * Date: 15-10-15
 * Time: 上午12:51
 */

class System
{
    /** @type  static */
    static $instance;

    public function init() {
        //$this->makeConfigConsts();

        //加载 C() 的 Key 定义
        Def::load();


        //C('ACTION_BIND_CLASS',true);

        // 类别名
        class_alias('Illuminate\Database\Eloquent\Model','Eloquent');

        define('SHOP_PATH', SITE_PATH.'shop'.DIRECTORY_SEPARATOR);
        set_include_path(SHOP_PATH . PATH_SEPARATOR . get_include_path());


        self::$instance = $this;
    }

    /**
     * @return static
     */
    public function app(){
        return self::$instance;
    }

    public function run(){
        //kd('test');
    }

    public static function makeConfigConsts(){
        $conf_keys=[];
        $class="<?php\n\nClass Def {\n\n";
        $config = C();
        ksort($config);
        $defines = '';
        foreach($config as $k => $v){
            $class.="\tconst $k = \"$k\";\n";
            $defines.="define(\"_{$k}\",\"$k\");\n";
        }
        $class .= "\n}";
        header("Content-Type: text/plain");
        exit($class."\n\n".$defines);

    }
}
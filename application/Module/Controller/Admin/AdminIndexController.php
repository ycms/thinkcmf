<?php namespace Controller\Admin;

use Controller\BaseController;
use Illuminate\Http\Request;
use Model\Repository\Admin\AdminIndex;
use Model\Repository\User;

/**
 * User: Mo
 * Date: 15-10-15
 * Time: 下午11:32
 */
class AdminIndexController extends BaseController
{
    public function main($action){
        global $_LANG, $smarty, $err, $ecs, $_CFG, $db, $sess;
        require MODULE_PATH.'/Lib/'.($action ?: 'index').'.php';
    }

    public function route(&$action,&$args){
        $action = 'main';
        $args = [strtolower(CONTROLLER_NAME)];
    }
}
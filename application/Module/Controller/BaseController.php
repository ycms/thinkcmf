<?php namespace Controller;

use Illuminate\Http\Request;

/**
 * User: Mo
 * Date: 15-10-15
 * Time: 上午5:13
 */
class BaseController
{
    public function render($tpl = '') {

    }

    public function error($msg, $code = 0) {
        if ($msg instanceof \Closure) {
            $msg = $msg();
        }
    }

    public function route(&$action = null, &$args = null) {
        //$model = "Model\\Repository\\".ucfirst(MODULE_NAME)."\\".ucfirst(MODULE_NAME).ucfirst(CONTROLLER_NAME);
        $request = New Request();
        $action = strtolower($request->method()) . ucfirst(ACTION_NAME);
        if (method_exists($this, $action)) {
            $args = [];
            $method = new \ReflectionMethod($this, $action);
            /** @type \ReflectionParameter $param */
            foreach ($method->getParameters() as $param) {
                $class = $param->getClass()->name;
                $args[] = new $class;
            }
        }

    }
}
<?php namespace Controller\Install;

use Controller\BaseController;

/**
 * User: Mo
 * Date: 15-10-16
 * Time: 下午7:17
 */
class InstallController extends BaseController
{
    public function main($action) {
        global $_LANG, $smarty, $err, $ecs, $_CFG, $db, $sess;

        require SHOP_PATH.'install/'.($action ?: 'index').'.inc.php';
    }

    public function route(&$action, &$args) {
        $action = 'main';
        $args = [strtolower(CONTROLLER_NAME)];
    }
}
<?php namespace Controller\Shop;

use App\Request;
use Controller\BaseController;

/**
 * User: Mo
 * Date: 15-10-16
 * Time: 上午2:21
 */
class ShopController extends BaseController
{
    public function all($action) {
        global $_LANG, $smarty, $err, $ecs, $_CFG, $db;
        require(MODULE_PATH . "Lib/$action.php");
    }

    public function route(&$action = null, &$args = null) {
        $action = 'all';
        $args = [strtolower(CONTROLLER_NAME), ACTION_NAME];
        return;
        //$model = "Model\\Repository\\".ucfirst(MODULE_NAME)."\\".ucfirst(MODULE_NAME).ucfirst(CONTROLLER_NAME);
        $request = New Request();
        $action = strtolower($request->method()) . ucfirst(ACTION_NAME);
        if (method_exists($this, $action)) {
            $args = [];
            $method = new \ReflectionMethod($this, $action);
            /** @type \ReflectionParameter $param */
            foreach ($method->getParameters() as $param) {
                $class = $param->getClass()->name;
                $args[] = new $class;
            }
        }

    }
}
<?php namespace Controller\Web\Home;

use App\Request;
use Controller\BaseController;
use Model\Repository\Api\ApiIndex;
use Model\Repository\Web\WebIndex;

/**
 * User: Mo
 * Date: 15-10-15
 * Time: 上午5:13
 */
class IndexController extends BaseController
{
    public $recentPosts;
    public $otherData;
    public $users;

    public function getIndex(WebIndex $data, Request $request) {
        $this->recentPosts = $data->fetchMyGoods($request->only(['id','type']),5);
        $this->otherData = $data->fetchMyGoods(5);
        $this->users = ApiIndex::fetchMyGoods($request->only('id'));
        if (empty($this->users)) {
            return $this->error($message = '', $code = 0);
        }
        return $this->render('web.home');
    }
}
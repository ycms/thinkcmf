<?php namespace Model\Entity;

use ReflectionObject;
use ReflectionProperty;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * 实体的原型,添加对数据库字段的自动支持以降低字段名的记忆成本
 * 除 Entity 这个原型之外, 每一个 Entity 应该对应数据库中的表
 * Created by PhpStorm.
 * User: Mo
 * Date: 15-10-15
 * Time: 上午2:25
 */
class Entity extends \Eloquent
{
    protected $fillable = [];
    protected $guarded = array();

    protected static $_db;
    private $_fields;

    /**
     * @param array $attr
     * @param int $count;
     */
    public function __construct(&$attr = [],&$count = 0) {
        $this->reset();
        
        if(is_numeric($attr)){
            $count = $attr;
            $attr = [];
        }
        
        if(is_string($attr)){
            parse_str($attr,$attr);
        }
        
        parent::__construct();
        
        foreach ($attr as $k => $v) {
            if($v === null or $v === ''){
                continue;
            }
            if ($this->fields($k)) {
                if((!empty($this->fillable) && in_array($k,$this->fillable)) or !in_array($k,$this->guarded) ){
                    $this->where($k, $v);
                }
            }
        }
        
        if(!empty($count)){
            $this->take(max(0,intval(strval($count))));
        }
        
    }

    /**
     * 添加 readXXX() 和 getAttr() 读属性事件
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        if (method_exists($this, $method = 'read' . ucfirst($name)) || method_exists($this, $method = 'getAttr')) {
            return $this->$method($name);
        } else {
            return $this->getAttribute($name);
        }
    }

    /**
     * 添加 writeXXX() 和 setAttr() 写属性事件的处理
     * @param string $name
     * @param null   $value
     */
    public function __set($name, $value = null) {
        if (method_exists($this, $method = 'write' . ucfirst($name)) || method_exists($this, $method = 'setAttr')) {
            $this->setAttribute($name, $this->$method($value, $name));
        } else {
            $this->setAttribute($name, $value);
        }
    }

    /**
     * 获取当前对象的字段名,排除默认的非数据库字段
     * 只有 public $xxx; 会被当做数据库字段
     * static $xxx; private $xxx; protected $xxx; 以及以下划线开始的 public $_xxx; 不会当做数据库字段;
     * @param string $key
     * @return mixed
     */
    public function fields($key = null) {
        if (!isset($this->_fields)) {
            $this->_fields = [];
            $ro = new \ReflectionClass(__CLASS__);
            $current = (new ReflectionObject($this))->getProperties(ReflectionProperty::IS_PUBLIC);
            $attr = array_diff($current, $ro->getProperties(ReflectionProperty::IS_PUBLIC));
            $publics = array_filter(
                $attr,
                function (ReflectionProperty $prop) {
                    return !$prop->isStatic() && $prop->name{0} != '_';
                }
            );
            foreach ($publics as $prop) {
                $this->_fields[$prop->name] = $this->{$prop->name};
            }
        }
        return isset($key) ? array_key_exists($key,$this->_fields) : $this->_fields;
    }

    /**
     * 删除对象的原始属性值,避免无法触发 __set() 和 __get()
     */
    public function reset() {
        foreach ($this->fields() as $property => $v) {
            unset($this->$property);
        }
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function t(){
        return DB::table($this->table);
    }

    /**
     * @return \Illuminate\Database\Capsule\Manager
     */
    public static function db(){
        return static::$_db ?: static::$_db = new DB;
    }

    public static function schema(){
        return call_user_func_array([static::db(),__FUNCTION__],func_get_args());
    }
}
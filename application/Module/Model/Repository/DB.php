<?php namespace Model\Repository;

/**
 * 辅助类
 * Created by PhpStorm.
 * User: Mo
 * Date: 15-10-15
 * Time: 上午3:23
 */
class DB
{
    /**
     * @param null  $data
     * @param array $param
     * @return \Model\Repository\User
     */
    public static function User(&$data = null, &$param = [], &$count = 0) {
        return $data = new User($param,$count);
    }

    /**
     * @param null  $data
     * @param array $param
     * @return \Model\Repository\Goods
     */
    public static function Goods(&$data = null, &$param = [], &$count = 0) {
        return $data = new Goods($param,$count);
    }
}

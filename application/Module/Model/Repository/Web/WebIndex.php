<?php namespace Model\Repository\Web;

use Model\Repository\DB;
use Model\Repository\Repository;
use Model\Repository\User;

/**
 * Created by PhpStorm.
 * User: Mo
 * Date: 15-10-15
 * Time: 上午3:37
 */
class WebIndex extends Repository
{

    /**
     * @param array                       $param
     * @param int                         $count
     * @param \Model\Repository\User|null $data
     * @return \Model\Repository\User
     */
    public static function fetchMyGoods($param = [], $count = 0, User &$data = null) {
        DB::User($data, $param, $count)->findOrNew([

        ]);

        return $data;
    }
}